# AWSS3

[![Software License][ico-license]](LICENSE.md)

A simple AWS S3 GET, PUT, DELETE implementation in PHP using AWS Rest interface


## Install

Via Composer

``` bash
$ composer require softsmart/awss3
```

## Usage

``` php

$object = new AWSS3();

$awsAccessKeyId = "MyAwsAccessKeyId";
$awsSecretAccessKey = "My/AwsSecret/Access/Key";
$bucketName = "MyBucketName";
$region = "us-east-2";

// GET an object
$return = $object->get('/dog.png', $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

if ($return["httpCode"] == 200) {
    file_put_contents(dirname(__FILE__)."/dog.png", $return["content"]);
    print "<img src=\"dog.png\">";
}


// PUT an object
$return = $object->put(dirname(__FILE__)."/dog.png", "/dog.png", $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);


// DELETE an object
$return = $object->delete('/dog.png', $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);
    
```


## Testing

``` bash
$ phpcs -c phpunit.xml
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.


## Credits

- [John McMurray][link-author]
- [All Contributors][link-contributors]

## License

GNU GENERAL PUBLIC LICENSE. Please see [License File](LICENSE.md) for more information.

[ico-license]: https://img.shields.io/aur/license/yaourt.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/:vendor/:package_name
[link-downloads]: https://packagist.org/packages/:vendor/:package_name
[link-author]: https://github.com/jsmcm
[link-contributors]: ../../contributors


## Questions

Q: Why not just use their SDK?

A: Its probably best but the SDK is for all Amazon products. It seems like overkill if all you want to do is add and delete files to an S3 bucket (eg, for remote images).

