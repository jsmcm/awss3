<?php
require dirname(__DIR__)."/src/AWSS3.php";

class AWSS3Test extends \PHPUnit\Framework\TestCase
{

	
	/***************************
	 * 
	 * GET TESTS
	 * 
	 **************************/
    public function testGetFailsIfFileNameBlank()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$fileName = "";
		$awsAccessKeyId = "b";
		$awsSecretAccessKey = "c";
		$bucketName = "d";
		$region = "e";

		$this->expectException("Exception");
        $this->expectExceptionCode(100);
		$this->expectExceptionMessage("fileName cannot be blank!");
		
		$result = $oAWSS3->get($fileName, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

    }


	        
    public function testGetFailsIfAwsAccessKeyIdBlank()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$fileName = "a";
		$awsAccessKeyId = "";
		$awsSecretAccessKey = "c";
		$bucketName = "d";
		$region = "e";

		$this->expectException("Exception");
        $this->expectExceptionCode(200);
		$this->expectExceptionMessage("awsAccessKeyId cannot be blank!");
		
		$result = $oAWSS3->get($fileName, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

	}	
	        
    public function testGetFailsIfAwsSecretAccessKeyBlank()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$fileName = "a";
		$awsAccessKeyId = "b";
		$awsSecretAccessKey = "";
		$bucketName = "d";
		$region = "e";

		$this->expectException("Exception");
        $this->expectExceptionCode(300);
		$this->expectExceptionMessage("awsSecretAccessKey cannot be blank!");
		
		$result = $oAWSS3->get($fileName, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

    }

	        
    public function testGetFailsIfBucketNameBlank()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$fileName = "a";
		$awsAccessKeyId = "b";
		$awsSecretAccessKey = "d";
		$bucketName = "";
		$region = "e";

		$this->expectException("Exception");
        $this->expectExceptionCode(400);
		$this->expectExceptionMessage("bucketName cannot be blank!");
		
		$result = $oAWSS3->get($fileName, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

    }

	        
    public function testGetFailsIfRegionBlank()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$fileName = "a";
		$awsAccessKeyId = "b";
		$awsSecretAccessKey = "c";
		$bucketName = "d";
		$region = "";

		$this->expectException("Exception");
        $this->expectExceptionCode(500);
		$this->expectExceptionMessage("region cannot be blank!");
		
		$result = $oAWSS3->get($fileName, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

    }






	/***************************
	 * 
	 * PUT TESTS
	 * 
	 **************************/

    public function testPutFailsIfLocalFileBlank()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$localFile = "";
		$awsPath = "b";
		$awsAccessKeyId = "c";
		$awsSecretAccessKey = "d";
		$bucketName = "e";
		$region = "f";

		$this->expectException("Exception");
        $this->expectExceptionCode(110);
		$this->expectExceptionMessage("localFile cannot be blank");
		
		$result = $oAWSS3->put($localFile, $awsPath, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

    }


    public function testPutFailsIfLocalFileDoesNotExist()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$localFile = "a";
		$awsPath = "b";
		$awsAccessKeyId = "c";
		$awsSecretAccessKey = "d";
		$bucketName = "e";
		$region = "f";

		$this->expectException("Exception");
        $this->expectExceptionCode(120);
		$this->expectExceptionMessage("localFile does not exist!");
		
		$result = $oAWSS3->put($localFile, $awsPath, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

	}
	
    public function testPutFailsIfAwsPathBlank()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$localFile = dirname(__FILE__)."/AWSS3Test.php";
		$awsPath = "";
		$awsAccessKeyId = "b";
		$awsSecretAccessKey = "c";
		$bucketName = "d";
		$region = "e";

		$this->expectException("Exception");
        $this->expectExceptionCode(100);
		$this->expectExceptionMessage("awsPath cannot be blank");
		
		$result = $oAWSS3->put($localFile, $awsPath, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

    }


	        
    public function testPutFailsIfAwsAccessKeyIdBlank()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$localFile = dirname(__FILE__)."/AWSS3Test.php";
		$awsPath = "b";
		$awsAccessKeyId = "";
		$awsSecretAccessKey = "d";
		$bucketName = "e";
		$region = "f";

		$this->expectException("Exception");
        $this->expectExceptionCode(200);
		$this->expectExceptionMessage("awsAccessKeyId cannot be blank!");
		
		$result = $oAWSS3->put($localFile, $awsPath, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

	}	
	        
    public function testPutFailsIfAwsSecretAccessKeyBlank()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$localFile = dirname(__FILE__)."/AWSS3Test.php";
		$awsPath = "b";
		$awsAccessKeyId = "c";
		$awsSecretAccessKey = "";
		$bucketName = "e";
		$region = "f";

		$this->expectException("Exception");
        $this->expectExceptionCode(300);
		$this->expectExceptionMessage("awsSecretAccessKey cannot be blank!");
		
		$result = $oAWSS3->put($localFile, $awsPath, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

    }

	        
    public function testPutFailsIfBucketNameBlank()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$localFile = dirname(__FILE__)."/AWSS3Test.php";
		$awsPath = "b";
		$awsAccessKeyId = "c";
		$awsSecretAccessKey = "d";
		$bucketName = "";
		$region = "f";

		$this->expectException("Exception");
        $this->expectExceptionCode(400);
		$this->expectExceptionMessage("bucketName cannot be blank!");
		
		$result = $oAWSS3->put($localFile, $awsPath, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

    }

	        
    public function testPutFailsIfRegionBlank()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$localFile = dirname(__FILE__)."/AWSS3Test.php";
		$awsPath = "b";
		$awsAccessKeyId = "c";
		$awsSecretAccessKey = "d";
		$bucketName = "e";
		$region = "";

		$this->expectException("Exception");
        $this->expectExceptionCode(500);
		$this->expectExceptionMessage("region cannot be blank!");
		
		$result = $oAWSS3->put($localFile, $awsPath, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

    }










	
	/***************************
	 * 
	 * DELETE TESTS
	 * 
	 **************************/
    public function testDeleteFailsIfFileNameBlank()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$fileName = "";
		$awsAccessKeyId = "b";
		$awsSecretAccessKey = "c";
		$bucketName = "d";
		$region = "e";

		$this->expectException("Exception");
        $this->expectExceptionCode(100);
		$this->expectExceptionMessage("fileName cannot be blank!");
		
		$result = $oAWSS3->delete($fileName, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

    }


	        
    public function testDeleteFailsIfAwsAccessKeyIdBlank()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$fileName = "a";
		$awsAccessKeyId = "";
		$awsSecretAccessKey = "c";
		$bucketName = "d";
		$region = "e";

		$this->expectException("Exception");
        $this->expectExceptionCode(200);
		$this->expectExceptionMessage("awsAccessKeyId cannot be blank!");
		
		$result = $oAWSS3->delete($fileName, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

	}	
	        
    public function testDeleteFailsIfAwsSecretAccessKeyBlank()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$fileName = "a";
		$awsAccessKeyId = "b";
		$awsSecretAccessKey = "";
		$bucketName = "d";
		$region = "e";

		$this->expectException("Exception");
        $this->expectExceptionCode(300);
		$this->expectExceptionMessage("awsSecretAccessKey cannot be blank!");
		
		$result = $oAWSS3->delete($fileName, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

    }

	        
    public function testDeleteFailsIfBucketNameBlank()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$fileName = "a";
		$awsAccessKeyId = "b";
		$awsSecretAccessKey = "d";
		$bucketName = "";
		$region = "e";

		$this->expectException("Exception");
        $this->expectExceptionCode(400);
		$this->expectExceptionMessage("bucketName cannot be blank!");
		
		$result = $oAWSS3->delete($fileName, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

    }

	        
    public function testDeleteFailsIfRegionBlank()
    {
		
		$oAWSS3 = new \SoftSmart\Storage\AWSS3();
		
		$fileName = "a";
		$awsAccessKeyId = "b";
		$awsSecretAccessKey = "c";
		$bucketName = "d";
		$region = "";

		$this->expectException("Exception");
        $this->expectExceptionCode(500);
		$this->expectExceptionMessage("region cannot be blank!");
		
		$result = $oAWSS3->delete($fileName, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

    }






}
