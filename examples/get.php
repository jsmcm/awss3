<?php

include dirname(__DIR__)."/src/AWSS3.php";

use \SoftSmart\Storage;
$object = new AWSS3();

$awsAccessKeyId = "MyAwsAccessKeyId";
$awsSecretAccessKey = "My/AwsSecret/Access/Key";
$bucketName = "MyBucketName";
$region = "us-east-2";

// GET an object
$return = $object->get('/dog.png', $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);

if ($return["httpCode"] == 200) {
    file_put_contents(dirname(__FILE__)."/dog.png", $return["content"]);
    print "<img src=\"dog.png\">";
}
