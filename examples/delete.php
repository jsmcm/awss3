<?php

include dirname(__DIR__)."/src/AWSS3.php";

use \SoftSmart\Storage;
$object = new AWSS3();


$awsAccessKeyId = "MyAwsAccessKeyId";
$awsSecretAccessKey = "My/AwsSecret/Access/Key";
$bucketName = "MyBucketName";
$region = "us-east-2";


// DELETE an object
$return = $object->delete('/dog.png', $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region);
    