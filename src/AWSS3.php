<?php
/**
* A class to read / write / delete to amazon S3 buckets.
*
* PHP version 7.2
*
* GNU General Public License v3
*
* @category  Storage
* @package   AWSS3
* @author    John McMurray <john@softsmart.co.za>
* @copyright 2020-2030 SoftSmart.co.za
* @license   gnu v3
* @link      https://softsmart.co.za/projects/aws-s3/
*/


//https://czak.pl/2015/09/15/s3-rest-api-with-curl.html
//https://stackoverflow.com/questions/58004329/how-to-send-curl-request-for-delete-image-from-s3-bucket-in-php
//https://usefulangle.com/post/34/aws-s3-upload-api-php-curl


namespace SoftSmart\Storage;

/**
* AWSS3.php
*
* @category  Storage
* @package   AWSS3
* @author    John McMurray <john@softsmart.co.za>
* @copyright 2020-2030 SoftSmart.co.za
* @license   gnu v3
* @link      http://softsmart.co.za/projects/aws-s3/
*/
class AWSS3
{

    /**
    * Constructor. 
    */
    function __construct() 
    {
    }


    /**
    * Get a resource from S3
    *
    * @param string $fileName           The full file name / path on aws,eg /newdir/otherdir/myfile.txt
    * @param string $awsAccessKeyId     Your AWS access key id
    * @param string $awsSecretAccessKey Your AWS secret access key
    * @param string $bucketName         The bucket name
    * @param string $region             The AWS region, eg, us-east-1
    * 
    * @return boolean
    */
    function get($fileName, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region)
    {

        if ($fileName == "") {
            throw new \Exception("fileName cannot be blank!", 100);
        }

        if (substr($fileName, 0, 1) != "/") {
            $fileName = "/".$fileName;
        }

        
        if ($awsAccessKeyId == "") {
            throw new \Exception("awsAccessKeyId cannot be blank!", 200);
        }


        if ($awsSecretAccessKey == "") {
            throw new \Exception("awsSecretAccessKey cannot be blank!", 300);
        }


        if ($bucketName == "") {
            throw new \Exception("bucketName cannot be blank!", 400);
        }


        if ($region == "") {
            throw new \Exception("region cannot be blank!", 500);
        }

        return $this->connectToAws($fileName, "", "", $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region, "GET");

    }



    /**
    * Put a resource to S3
    *
    * @param string $localFile          The full file name / path on the local drive
    * @param string $awsPath            The full file name / path on AWS
    * @param string $awsAccessKeyId     Your AWS access key id
    * @param string $awsSecretAccessKey Your AWS secret access key
    * @param string $bucketName         The bucket name
    * @param string $region             The AWS region, eg, us-east-1
    * 
    * @return boolean
    */
    function put($localFile, $awsPath, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region)
    {

        if ($localFile == "") {
            throw new \Exception("localFile cannot be blank", 110);
        }

        if (!file_exists($localFile)) {
            throw new \Exception("localFile does not exist!", 120);
        }

        if ($awsPath == "") {
            throw new \Exception("awsPath cannot be blank", 100);
        }

        if (substr($awsPath, 0, 1) != "/") {
            $awsPath = "/".$awsPath;
        }

        
        if ($awsAccessKeyId == "") {
            throw new \Exception("awsAccessKeyId cannot be blank!", 200);
        }


        if ($awsSecretAccessKey == "") {
            throw new \Exception("awsSecretAccessKey cannot be blank!", 300);
        }


        if ($bucketName == "") {
            throw new \Exception("bucketName cannot be blank!", 400);
        }


        if ($region == "") {
            throw new \Exception("region cannot be blank!", 500);
        }


        $content = file_get_contents($localFile);
        
        $contentType = mime_content_type ($localFile);

        return $this->connectToAws($awsPath, $content, $contentType, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region, "PUT");

    }


    /**
    * DELETE a resource from S3
    *
    * @param string $fileName           The full file name / path on aws,eg /newdir/otherdir/myfile.txt
    * @param string $awsAccessKeyId     Your AWS access key id
    * @param string $awsSecretAccessKey Your AWS secret access key
    * @param string $bucketName         The bucket name
    * @param string $region             The AWS region, eg, us-east-1
    * 
    * @return boolean
    */
    function delete($fileName, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region)
    {

        if ($fileName == "") {
            throw new \Exception("fileName cannot be blank!", 100);
        }

        if (substr($fileName, 0, 1) != "/") {
            $fileName = "/".$fileName;
        }


        if ($awsAccessKeyId == "") {
            throw new \Exception("awsAccessKeyId cannot be blank!", 200);
        }


        if ($awsSecretAccessKey == "") {
            throw new \Exception("awsSecretAccessKey cannot be blank!", 300);
        }


        if ($bucketName == "") {
            throw new \Exception("bucketName cannot be blank!", 400);
        }


        if ($region == "") {
            throw new \Exception("region cannot be blank!", 500);
        }

        return $this->connectToAws($fileName, "", "", $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region, "DELETE");

    }


    /**
    * Connect to AWS and do the work
    *
    * @param string $fileName           The full file name / path on AWS (either the name to GET or DELETE or the name to PUT)
    * @param string $content            The content of the file for a put command (not the file path, the actual content)
    * @param string $contentType        The content type. Important because otherwise AWS download instead of returning the file (eg for images)
    * @param string $awsAccessKeyId     Your AWS access key id
    * @param string $awsSecretAccessKey Your AWS secret access key
    * @param string $bucketName         The bucket name
    * @param string $region             The AWS region, eg, us-east-1
    * 
    * @return boolean
    */
    private function connectToAws($fileName, $content, $contentType, $awsAccessKeyId, $awsSecretAccessKey, $bucketName, $region, $method)
    {

        $fileName = str_replace(" ", "%20", $fileName);
        

        $hostName = $bucketName . '.s3.'.$region.'.amazonaws.com';

        $content_acl = 'public-read';
        $aws_service_name = 's3';

        $timestamp = gmdate('Ymd\THis\Z');
        $date = gmdate('Ymd');

        $request_headers = array();

        if ($method == "PUT") {
            $request_headers['Content-Type'] = $contentType;
        }

        $request_headers['Date'] = $timestamp;
        $request_headers['Host'] = $hostName;
        $request_headers['x-amz-acl'] = $content_acl;
        $request_headers['x-amz-content-sha256'] = hash('sha256',$content);
        ksort($request_headers);

        $canonical_headers = [];
        foreach($request_headers as $key => $value)
        {
            $canonical_headers[] = strtolower($key) . ":" . $value;
        }
        $canonical_headers = implode("\n", $canonical_headers);

        $signed_headers = [];
        foreach($request_headers as $key => $value)
        {
            $signed_headers[] = strtolower($key);
        }
        $signed_headers = implode(";", $signed_headers);

        $canonical_request = [];
        $canonical_request[] = $method;
        $canonical_request[] = $fileName;
        $canonical_request[] = "";
        $canonical_request[] = $canonical_headers;
        $canonical_request[] = "";
        $canonical_request[] = $signed_headers;
        $canonical_request[] = hash('sha256', $content);
        $canonical_request = implode("\n", $canonical_request);
        $hashed_canonical_request = hash('sha256', $canonical_request);

        $scope = [];
        $scope[] = $date;
        $scope[] = $region;
        $scope[] = $aws_service_name;
        $scope[] = "aws4_request";

        $string_to_sign = [];
        $string_to_sign[] = "AWS4-HMAC-SHA256";
        $string_to_sign[] = $timestamp;
        $string_to_sign[] = implode('/', $scope);
        $string_to_sign[] = $hashed_canonical_request;
        $string_to_sign = implode("\n", $string_to_sign);

        $kSecret = 'AWS4' . $awsSecretAccessKey;
        $kDate = hash_hmac('sha256', $date, $kSecret, true);
        $kRegion = hash_hmac('sha256', $region, $kDate, true);
        $kService = hash_hmac('sha256', $aws_service_name, $kRegion, true);
        $kSigning = hash_hmac('sha256', 'aws4_request', $kService, true);

        $signature = hash_hmac('sha256', $string_to_sign, $kSigning);

        $authorization = [
            'Credential=' . $awsAccessKeyId . '/' . implode('/', $scope),
            'SignedHeaders=' . $signed_headers,
            'Signature=' . $signature
        ];
        $authorization = 'AWS4-HMAC-SHA256' . ' ' . implode( ',', $authorization);

        $curl_headers = [ 'Authorization: ' . $authorization ];
        foreach($request_headers as $key => $value)
        {
            $curl_headers[] = $key . ": " . $value;
        }
        

        $url = 'https://' . $hostName . $fileName;
        $ch = curl_init($url);

        if ($method != "GET") {
            if ($method == "PUT") {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
            }

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }
        

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);




        $return = curl_exec($ch); // return response data 1
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return [
            "httpCode" => $http_code,
            "content"  => $return
        ];

    }
    
}
